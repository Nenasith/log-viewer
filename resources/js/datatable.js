/**
 * Created by janweskamp on 14.07.17.
 */

$(document).ready(function () {

    $('#table-log').DataTable({
        'columnDefs': [
            {
                'orderable': false, 'targets': 3
            },
            {
                'orderable': false, 'targets': 4
            },
        ],
        "order": [1, 'desc'],
        "stateSave": true,
        "stateSaveCallback": function (settings, data) {
            window.localStorage.setItem("datatable", JSON.stringify(data));
        },
        "stateLoadCallback": function (settings) {
            let data = JSON.parse(window.localStorage.getItem("datatable"));
            if (data) {
                data.start = 0;
            }
            return data;
        }
    });

});