/**
 * Created by janweskamp on 14.07.17.
 */
$(document).ready(function () {

    $('.box-body').on('click', '.expand', function () {
        $('#' + $(this).data('display')).toggle();
    });

    $('.panel-body').on('click', '.expand', function () {
        console.log('expanded');
        $('#' + $(this).data('display')).toggle();
    });

});