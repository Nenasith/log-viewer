const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Log-Viewer
 | Created by janweskamp on 14.06.2017
 |--------------------------------------------------------------------------
 */

//COMPILE SASS TO CSS
mix.sass('resources/sass/log_viewer.scss', 'src/resources/assets/css/log_viewer/style.css');

//COMPILE JS FILES
mix.js(
    [
        'resources/js/datatable.js',
        'resources/js/delete-log.js',
        'resources/js/open-log.js',
    ],
    'src/resources/assets/js/log_viewer/log_viewer.js'
);

//MIX STYLES
mix.styles([
    //CUSTOM
    'src/resources/assets/css/log_viewer/style.css',
    //LIB
    'node_modules/datatables.net-bs/css/dataTables.bootstrap.css'
], 'src/resources/assets/css/log_viewer/log_viewer.css');
