<?php

namespace Nenasith\LogViewer;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Routing\Router;
use Nenasith\LogViewer\Http\Middleware\VerifyAdmin;

class ServiceProvider extends BaseServiceProvider
{
    
    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        include __DIR__ . '/routes/admin.php';
    }
    
    /**
     * Perform post-registration booting of services.
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        require __DIR__ . '/routes/admin.php';
        
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'log_viewer_views');
        
        $this->publishes([
            __DIR__ . '/config/log_viewer.php' => config_path('log_viewer.php'),
        ], 'config');
        
        $this->publishes([
            __DIR__ . '/resources/assets/js/' => public_path('js'),
        ], 'js');
        
        $this->publishes([
            __DIR__ . '/resources/assets/css/' => public_path('css'),
        ], 'css');
        
        $router->aliasMiddleware('VerifyAdmin', VerifyAdmin::class);
    }
}