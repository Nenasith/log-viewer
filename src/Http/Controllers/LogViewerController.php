<?php

namespace Nenasith\LogViewer\Controllers;

use Nenasith\LogViewer\Services\LaravelLogViewerService;
use \Illuminate\Routing\Controller as BaseController;

class LogViewerController extends BaseController
{
    
    /**
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    public function index()
    {
        if (request('l')) {
            LaravelLogViewerService::setFile(base64_decode(request('l')));
        }
        
        if (request('dl')) {
            return $this->download(LaravelLogViewerService::path(base64_decode(request('dl'))));
        } elseif (request()->has('del')) {
            app('files')->delete(LaravelLogViewerService::path(base64_decode(request('del'))));
            
            return redirect(request()->url());
        } elseif (request()->has('delall')) {
            foreach (LaravelLogViewerService::getFiles(true) as $file) {
                app('files')->delete(sprintf('%s/%s', storage_path('logs'), $file));
            }
            
            return redirect()->to(route('admin.log_viewer.index'));
        }
        
        return view('log_viewer_views::logs', [
            'logs'         => LaravelLogViewerService::all(),
            'files'        => LaravelLogViewerService::getFiles(true),
            'current_file' => LaravelLogViewerService::getFileName(),
        ]);
    }
    
    /**
     * @param $data
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    private function download($data)
    {
        return response()->download($data);
    }
}
