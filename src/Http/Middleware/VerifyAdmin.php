<?php

namespace Nenasith\LogViewer\Http\Middleware;

use Auth;
use Closure;

class VerifyAdmin
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return \Redirect
     */
    public function handle($request, Closure $next)
    {
        $access_roles = config('log_viewer.allowed_roles');
        $developers = config('log_viewer.developer_access');
        $access = true;
        
        if (is_null($access_roles) || is_null($developers)) {
            flash('You missed publishing the config file!')->error();
            
            return redirect()->to('/admin', 302);
        }
        
        if (empty(Auth::user())) {
            return redirect()->to('/login');
        }
        
        if (!in_array(Auth::user()->role, $access_roles, true) && !in_array(Auth::user()->email, $developers, true)) {
            $access = false;
        }
        
        if ($access) {
            return $next($request);
        } else {
            flash('You don\'t have access')->error();
            
            return redirect()->to('/admin', 302);
        }
    }
}
