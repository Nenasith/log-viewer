<?php

Route::group(
    [
        'prefix'     => 'admin/log-viewer',
        'namespace'  => 'Nenasith\LogViewer\Controllers',
        'middleware' => ['web', 'VerifyAdmin'],
        'as'         => 'admin.log_viewer.',
    ],
    function () {
        Route::get('/', [
            'uses'       => 'LogViewerController@index',
            'as'         => 'index',
        ]);
    }
);
