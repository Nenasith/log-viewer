<?php

return [
    'master_template'    => 'layouts.admin',
    'allowed_roles'      => ['ADMIN'],
    'developer_access'   => ['jan.weskamp@jtl-software.com'],
    'meta_section'       => 'meta',
    'css_section'        => 'css',
    'content_section'    => 'content',
    'javascript_section' => 'javascript',
    'admin_lte'          => false,
    'secure_assets'      => false,
    'font_awesome_cdn'   => false,
    'datatables_cdn'     => false,
];
