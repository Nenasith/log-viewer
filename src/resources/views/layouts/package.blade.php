@extends( config('log_viewer.master_template') !== null ? config('log_viewer.master_template') : 'layouts.app')

@section(config('log_viewer.meta_section') !== null ? config('log_viewer.meta_section') : 'meta')
    @yield('package_meta')
@endsection

@section(config('log_viewer.css_section') !== null ? config('log_viewer.css_section') : 'css')

    @if(config('log_viewer.font_awesome_cdn'))
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
              rel="stylesheet"
              integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
              crossorigin="anonymous">
    @endif

    @if(config('log_viewer.datatables_cdn'))
        <link rel="stylesheet"
              type="text/css"
              href="https://cdn.datatables.net/v/bs-3.3.7/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/>
    @endif

    @yield('package_css')
@endsection

@section(config('log_viewer.content_section') !== null ? config('log_viewer.content_section') : 'content')
    <div id="log_viewer-content">
        @yield('package_content')
    </div>
@endsection

@section(config('log_viewer.javascript_section') !== null ? config('log_viewer.javascript_section') : 'javascript')

    @if(config('log_viewer.datatables_cdn'))
        <script type="text/javascript"
                src="https://cdn.datatables.net/v/bs-3.3.7/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.js"></script>
    @endif

    @yield('package_javascript')
@endsection


