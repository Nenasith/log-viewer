@extends('log_viewer_views::layouts.package')

@section('package_css')
    <link rel="stylesheet" href="{{asset('/css/log_viewer/log_viewer.css',config('log_viewer.secure_assets'))}}">
@endsection

@section('package_content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="@if(config('log_viewer.admin_lte')) box box-primary @else panel panel-default @endif">
                    <div class="@if(config('log_viewer.admin_lte')) box-header @else panel-heading @endif">
                        <h1 class="@if(config('log_viewer.admin_lte')) box-title @else panel-title @endif">
                            <span class="fa fa-file-archive-o" aria-hidden="true"></span> LogFiles
                        </h1>
                    </div>
                    <div class="@if(config('log_viewer.admin_lte')) box-body @else panel-body @endif">
                        <div class="col-sm-3 col-md-2">
                            <div class="list-group">
                                @foreach($files as $file)
                                    <a href="/admin/log-viewer?l={{ base64_encode($file) }}"
                                       class="list-group-item @if ($current_file == $file) llv-active @endif">
                                        {{$file}}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-10">
                            @if ($logs === null)
                                <div>
                                    The Log file is > 50MB, please download it.
                                </div>
                            @else
                                <table id="table-log" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Level</th>
                                        <th>Context</th>
                                        <th>Date</th>
                                        <th>Content</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($logs as $key => $log)
                                        <tr>
                                            <td class="text-{{$log['level_class']}}"><span
                                                        class="glyphicon glyphicon-{{$log['level_img']}}-sign"
                                                        aria-hidden="true"></span> &nbsp{{$log['level']}}</td>
                                            <td class="text">
                                                {{$log['context']}}
                                            </td>
                                            <td class="date">
                                                {{$log['date']}}
                                            </td>
                                            <td class="text">
                                                {{$log['text']}}
                                                @if (isset($log['in_file']))
                                                    <br/>{{$log['in_file']}}
                                                @endif
                                                @if ($log['stack'])
                                                    <div class="stack" id="stack{{$key}}"
                                                         style="display: none; white-space: pre-wrap;">
                                                        {{ trim($log['stack']) }}
                                                    </div>
                                                @endif
                                            </td>
                                            <td class="actions">
                                                @if ($log['stack'])
                                                    <a class="pull-right expand btn btn-default btn-xs"
                                                       data-display="stack{{$key}}">
                                                        <span class="fa fa-search"></span>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                            <div>
                                @if($current_file)
                                    <a href="/admin/log-viewer?dl={{ base64_encode($current_file) }}">
                                        <span class="fa fa-download"></span> Download file
                                    </a>
                                    -
                                    <a id="delete-log" href="/admin/log-viewer?del={{ base64_encode($current_file) }}">
                                        <span class="fa fa-trash"></span> Delete file
                                    </a>
                                    @if(count($files) > 1)
                                        -
                                        <a id="delete-all-log" href="/admin/log-viewer?delall=true">
                                            <span class="fa fa-trash"></span>
                                            Delete all files
                                        </a>
                                    @endif
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('package_javascript')
    <script src="{{asset('/js/log_viewer/log_viewer.js',config('log_viewer.secure_assets'))}}"></script>
@endsection

