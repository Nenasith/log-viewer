
# Log-Viewer Version: 1.0.13

## Requirements

1. Laravel 5.5+
2. PHP 7.0
3. Jquery
4. Bootstrap
5. Vue (optional)
6. AdminLTE (optional)

## Composer

Run  this 

```
composer require nenasith/log-viewer
```

## Publish

Then run ```composer update && php artisan vendor:publish``` 
After that migrate the database ```php artisan migrate```

## Layout

Maybe you have to change your layout app.blade.php!

Be sure you have a ``` @yield('meta') ``` area below your base script.
A ``` @yield('content') ```, ``` @yield('css') ``` area and a ``` @yield('javascript') ``` in your footer section.

## Useage

Visit ```https://yourdoamin.de/admin/log-viewer``` to see the log viewer!

## Package settings

There is an ```log_viewer.php``` file in the config folder.

```
<?php

return [
    'master_template'    => 'layouts.admin',
    'allowed_roles'      => ['ADMIN'],
    'developer_access'   => ['mail@mail.com'],
    'meta_section'       => 'meta',
    'css_section'        => 'css',
    'javascript_section' => 'javascript',   
    'content_section'    => 'content',
    'admin_lte'          => false,
    'secure_assets'      => true,
    'font_awesome_cdn'   => false,
    'datatables_cdn'     => false,
];

```

Here you can edit some settings for the middlewares and views.

## Package styles & js files
Run this commands in your package dir if you want to refresh scss / js files

```
npm install && npm run dev

```